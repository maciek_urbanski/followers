import os

from celery import Celery
from flask import Flask


def make_celery(app):
    celery = Celery(
        app.import_name,
        backend=app.config['CELERY_RESULT_BACKEND'],
        broker=app.config['CELERY_BROKER_URL'],
    )
    celery.conf.update(app.config)
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery.Task = ContextTask
    return celery


def make_app():
    app = Flask(__name__)
    app.secret_key = 'secret'
    app.session_cookie_name = 'flask_session'

    app.config.update(
        CELERY_BROKER_URL=os.environ['REDIS_URL'],
        CELERY_RESULT_BACKEND=os.environ['REDIS_URL'],
    )
    return app

app = make_app()
celery = make_celery(app)

from app.controllers import followers_app
app.register_blueprint(followers_app)
