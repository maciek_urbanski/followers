from flask import (
    Blueprint,
    jsonify,
    redirect,
    render_template,
    request,
    session,
)

from app.workers import (
    get_async_result,
    get_followers_ids,
)
from app.twitter_client import twitter_oauth_client


followers_app = Blueprint(
    'followers_app',
    __name__,
    template_folder='templates',
)


@followers_app.route('/')
def index():
    return render_template('index.html')


@followers_app.route('/auth')
def auth():
    url, request_token = twitter_oauth_client.get_authorization_url()
    session['request_token'] = request_token
    return redirect(url)


@followers_app.route('/callback')
def twitter_callback():
    session['oauth_token'] = request.args.get('oauth_token')
    session['oauth_verifier'] = request.args.get('oauth_verifier')
    return redirect('/followers/followers')


@followers_app.route('/followers/followers')
def followers():
    request_token = session['oauth_token']
    oauth_verifier = session['oauth_verifier']
    async_res = get_followers_ids.apply_async(
        (request_token, oauth_verifier),
        serializer='json',
    )
    session['TASK_ID'] = async_res.id
    return render_template('loading.html')


@followers_app.route('/loading')
def check_if_complete():
    async_res_id = session.get('TASK_ID')
    if async_res_id is None:
        jsonify({'status': 'RUNNING', 'result': 'Loading ...'})
    async_res = get_async_result(async_res_id)

    if async_res.failed():
        return jsonify({'status': 'ERROR', 'result': 'Try again'})
    elif async_res.successful():
        result = async_res.get()
        session['followers'] = result
        return jsonify({
            'status': 'OK',
            'result': render_template(
                'template.html',
                data=result
            )
        })
    else:
        return jsonify({'status': 'RUNNING', 'result': 'Loading ...'})


@followers_app.route('/followers_json')
def followers_json():
    data = session['followers']
    return jsonify(data)
