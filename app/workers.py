import tweepy

from celery.result import AsyncResult

from app import celery
from app.twitter_client import twitter_oauth_client


@celery.task(throws=tweepy.error.TweepError)
def get_followers_ids(oauth_token, oauth_verifier):
    client = twitter_oauth_client.get_user_authorized_client(
        oauth_token, oauth_verifier)
    return client.get_2nd_line_followers_with_number_of_1st_line_followers()


def get_async_result(async_res_id):
    return AsyncResult(async_res_id, app=celery)
