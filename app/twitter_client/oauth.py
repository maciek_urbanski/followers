import tweepy


class TwitterOAuthClient:

    def __init__(self, consumer_key, consumer_secret, callback_url):
        self._consumer_key = consumer_key
        self._consumer_secret = consumer_secret
        self._callback_url = callback_url

    def get_authorization_url(self):
        auth = tweepy.OAuthHandler(
            self._consumer_key,
            self._consumer_secret,
            self._callback_url,
        )
        authorization_url = auth.get_authorization_url()
        return authorization_url, auth.request_token
