

class ExtractIdsWithListOfIdsTheyFollow:

    def __call__(self, ids_with_follower_ids):
        ids_with_ids_they_follow = dict()
        for id, follower_ids in ids_with_follower_ids.items():
            for follower_id in follower_ids:
                ids_with_ids_they_follow.setdefault(
                    follower_id, []).append(id)
        return ids_with_ids_they_follow


class FilterOutSecondLineFollowerIdsIfTheyExistInFirstLine:

    def __call__(self, ids_with_follower_ids, ids_with_following_ids):
        filtered_ids_with_following_ids = dict()
        for follower_id, following_ids in ids_with_following_ids.items():
            if follower_id in ids_with_follower_ids.keys():
                continue
            filtered_ids_with_following_ids[follower_id] = following_ids
        return filtered_ids_with_following_ids


class PrepareNamesWithFollowindIdsCount:

    def __call__(self, ids_with_following_ids, ids_name_map):
        names_with_count = dict()
        for id, following_ids in ids_with_following_ids.items():
            names_with_count[ids_name_map[id]] = len(following_ids)
        return names_with_count
