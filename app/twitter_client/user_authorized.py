import time

import tweepy

from .data_transformers import (
    ExtractIdsWithListOfIdsTheyFollow,
    FilterOutSecondLineFollowerIdsIfTheyExistInFirstLine,
    PrepareNamesWithFollowindIdsCount,
)


class TwitterUserAuthorizedClient:

    def __init__(self, tweeter_api):
        self._api = tweeter_api

    def get_2nd_line_followers_with_number_of_1st_line_followers(self):
        user_id = self._api.me().id
        first_line_followers = self._get_followers_ids(user_id)
        first_line_followers_with_second_line = \
            self._get_second_line_followers(first_line_followers)

        second_line_followers_with_following_ids = \
            ExtractIdsWithListOfIdsTheyFollow()(
                first_line_followers_with_second_line,
            )

        without_second_line_followers_in_first = \
            FilterOutSecondLineFollowerIdsIfTheyExistInFirstLine()(
                first_line_followers_with_second_line,
                second_line_followers_with_following_ids,
            )

        ids_names_map = self._get_user_names_for_ids(
            without_second_line_followers_in_first,
        )

        return PrepareNamesWithFollowindIdsCount()(
            without_second_line_followers_in_first,
            ids_names_map,
        )

    def _get_followers_ids(self, user_id):
        followers_ids = []
        cursor = tweepy.Cursor(
            self._api.followers_ids, user_id=user_id, count=5000).pages()
        for ids in limit_handled(cursor):
            followers_ids.extend(ids)
        return followers_ids

    def _get_second_line_followers(self, first_followers_ids):
        second_followers_ids = {}
        for first_follower_id in first_followers_ids:
            second_followers_ids.setdefault(
                first_follower_id, []
            ).extend(
                self._get_followers_ids(first_follower_id)
            )
        return second_followers_ids

    def _get_user_names_for_ids(self, ids):
        ids_names_map = dict()
        for id in ids:
            try:
                user = self._api.get_user(id)
            except tweepy.RateLimitError:
                time.sleep(15 * 60)
                user = self._api.get_user(id)
            ids_names_map[id] = user.screen_name
        return ids_names_map


def limit_handled(cursor):
    while True:
        try:
            yield cursor.next()
        except tweepy.RateLimitError:
            time.sleep(15 * 60)
