import pytest

from app.twitter_client.data_transformers import (
    ExtractIdsWithListOfIdsTheyFollow,
    FilterOutSecondLineFollowerIdsIfTheyExistInFirstLine,
    PrepareNamesWithFollowindIdsCount,
)


class TestExtractIdsWithListOfIdsTheyFollow:

    def test_data_extracted(
            self, ids_with_follower_ids, ids_with_following_ids):
        result = ExtractIdsWithListOfIdsTheyFollow()(ids_with_follower_ids)
        assert result == ids_with_following_ids


class TestFilterOutSecondLineFollowerIdsIfTheyExistInFirstLine:

    def test_filter_out_if_follower_id_in_following_id(
        self,
        ids_with_follower_ids_follower_on_both_sides,
        ids_with_following_ids_follower_on_both_sides,
        filtered_ids_with_following_ids_follower_on_both_sides,
    ):
        result = FilterOutSecondLineFollowerIdsIfTheyExistInFirstLine()(
            ids_with_follower_ids_follower_on_both_sides,
            ids_with_following_ids_follower_on_both_sides,
        )
        assert result == \
            filtered_ids_with_following_ids_follower_on_both_sides

    def test_return_same_data_if_none_of_follower_ids_in_following_ids(
            self, ids_with_follower_ids, ids_with_following_ids):
        result = FilterOutSecondLineFollowerIdsIfTheyExistInFirstLine()(
            ids_with_follower_ids, ids_with_following_ids)
        assert result == ids_with_following_ids


class TestPrepareNamesWithFollowindIdsCount:

    @pytest.fixture
    def ids_names_mapping(self):
        return {
            345: 'Mark',
            678: 'Bob',
            901: 'Rick',
            876: 'Twitter',
        }

    def test_prepared(self, ids_with_following_ids, ids_names_mapping):
        result = PrepareNamesWithFollowindIdsCount()(
            ids_with_following_ids, ids_names_mapping)
        assert result == {
            'Mark': 2,
            'Bob': 3,
            'Rick': 2,
            'Twitter': 1,
        }
