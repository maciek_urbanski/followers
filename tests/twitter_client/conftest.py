import pytest


@pytest.fixture
def ids_with_follower_ids():
    return {
        123: [345, 678, 901],
        321: [345, 678, 876],
        231: [678, 901],
    }


@pytest.fixture
def ids_with_follower_ids_follower_on_both_sides():
    return {
        123: [345, 678, 901],
        321: [345, 678, 876],
        345: [678, 901],
    }


@pytest.fixture
def ids_with_following_ids():
    return {
        345: [123, 321],
        678: [123, 321, 231],
        901: [123, 231],
        876: [321],
    }


@pytest.fixture
def ids_with_following_ids_follower_on_both_sides():
    return {
        345: [123, 321],
        678: [123, 321, 231],
        901: [123, 345],
        876: [321],
    }


@pytest.fixture
def filtered_ids_with_following_ids_follower_on_both_sides():
    return {
        678: [123, 321, 231],
        901: [123, 345],
        876: [321],
    }
