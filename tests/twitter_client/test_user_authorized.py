import mock
import pytest

from app.twitter_client.user_authorized import TwitterUserAuthorizedClient


class TestTwitterUserAuthorizedClient:

    @pytest.fixture
    def api(self):
        class User:
            def __init__(self, id, name=None):
                self.id = id
                self.screen_name = name

        class Api:
            def me(self):
                return User(555)

            def get_user(self, id):
                data = {
                    345: User(345, 'Mark'),
                    678: User(678, 'Bob'),
                    901: User(901, 'Rick'),
                    876: User(876, 'Twitter'),
                }
                return data.get(id)
        return Api()

    @pytest.fixture
    def client(self, api):
        return TwitterUserAuthorizedClient(api)

    @pytest.yield_fixture
    def mocked_get_followers_ids(self, ids_with_follower_ids):
        data = {
            555: [123, 321, 231],
        }
        data.update(ids_with_follower_ids)
        with mock.patch.object(
            TwitterUserAuthorizedClient,
            '_get_followers_ids'
        ) as get:
            get.side_effect = data.values()
            yield

    @pytest.mark.usefixtures('mocked_get_followers_ids')
    def test_get_2nd_line_followers_with_number_of_1st_line_followers(
            self, client, second_line_followers_with_count):
        result = \
            client.get_2nd_line_followers_with_number_of_1st_line_followers()
        assert result == second_line_followers_with_count
