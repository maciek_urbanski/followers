import pytest

from app import make_app


@pytest.fixture
def app():
    app = make_app()
    from app.controllers import followers_app
    app.register_blueprint(followers_app)
    return app


@pytest.fixture
def redirect_url():
    return 'https://api.twitter.com/oauth/authorize?'\
        'oauth_token=ovLhqgAAAAAA0o9oAAABXEljpJc'


@pytest.fixture
def oauth_token():
    return 'ovLhqgAAAAAA0o9oAAABXEljpJc'


@pytest.fixture
def oauth_verifier():
    return'OfTMd9k5Dk3pj2wfBAOSaRA4itQQ32'


@pytest.fixture
def second_line_followers_with_count():
    return {
        'Mark': 2,
        'Bob': 3,
        'Rick': 2,
        'Twitter': 1,
    }
