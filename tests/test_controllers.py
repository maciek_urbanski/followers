import mock
import pytest

from celery.result import AsyncResult

from app.twitter_client import TwitterOAuthClient


class TestAuthorization:

    @pytest.yield_fixture
    def mock_twitter_api(self, redirect_url, oauth_token):
        with mock.patch.object(
                TwitterOAuthClient, 'get_authorization_url') as get_url:
            get_url.return_value = redirect_url, oauth_token
            yield

    @pytest.mark.usefixtures('mock_twitter_api')
    def test_redirect_to_authorization_url(
            self, client, redirect_url, oauth_token):
        response = client.get('/auth')
        with client.session_transaction() as session:
            assert session['request_token'] == oauth_token
        assert response.status_code == 302
        assert response.headers.get('location') == redirect_url

    def test_redirect_after_callback(
            self, client, oauth_token, oauth_verifier):
        response = client.get(
            '/callback?oauth_token=%s&oauth_verifier=%s' % (
                oauth_token,
                oauth_verifier,
            )
        )
        with client.session_transaction() as session:
            assert session['oauth_token'] == oauth_token
            assert session['oauth_verifier'] == oauth_verifier
        assert response.status_code == 302
        assert response.headers.get('location') == \
            'http://localhost/followers/followers'


class TestGetFollowers:

    @pytest.fixture
    def failed_async_result(self):
        result = mock.Mock(spec=AsyncResult)
        result.failed.return_value = True
        return result

    @pytest.fixture
    def loading_async_result(self):
        result = mock.Mock(spec=AsyncResult)
        result.failed.return_value = False
        result.successful.return_value = False
        return result

    @pytest.fixture
    def successful_async_result(self, second_line_followers_with_count):
        result = mock.Mock(spec=AsyncResult)
        result.failed.return_value = False
        result.successful.return_value = True
        result.get.return_value = second_line_followers_with_count
        return result

    @pytest.fixture
    def set_task_id_in_session(self, client):
        with client.session_transaction() as session:
            session['TASK_ID'] = 12345

    @pytest.fixture
    def set_followers_in_session(
            self, client, second_line_followers_with_count):
        with client.session_transaction() as session:
            session['followers'] = second_line_followers_with_count

    @pytest.fixture
    def set_oauth_params_in_session(
            self, client, oauth_token, oauth_verifier):
        with client.session_transaction() as session:
            session['oauth_token'] = oauth_token
            session['oauth_verifier'] = oauth_verifier

    @pytest.mark.usefixtures('set_oauth_params_in_session')
    def test_initialize_get_followers(self, client):
        with mock.patch('app.workers.get_followers_ids.apply_async') as async:
            async.return_value = mock.Mock(id=12345, spec=AsyncResult)
            response = client.get('/followers/followers')
            assert response.status_code == 200
            assert '<title>Result</title>' in response.get_data(as_text=True)

        with client.session_transaction() as session:
            assert session['TASK_ID'] == 12345

    @pytest.mark.usefixtures('set_followers_in_session')
    def test_followers_json(self, client, second_line_followers_with_count):
        response = client.get('/followers_json')
        assert response.status_code == 200
        assert response.json == second_line_followers_with_count

    @pytest.mark.usefixtures('set_task_id_in_session')
    def test_result_ok(
        self, client, successful_async_result,
        second_line_followers_with_count
    ):
        with mock.patch('app.controllers.get_async_result') as async:
            async.return_value = successful_async_result
            response = client.get('/loading')
            assert response.status_code == 200
            assert response.json['status'] == 'OK'
            assert all(
                [key in response.json['result']
                 for key in second_line_followers_with_count.keys()]
            )

        with client.session_transaction() as session:
            assert session['followers'] == second_line_followers_with_count

    @pytest.mark.usefixtures('set_task_id_in_session')
    def test_result_running(self, client, loading_async_result):
        with mock.patch('app.controllers.get_async_result') as async:
            async.return_value = loading_async_result
            response = client.get('/loading')
            assert response.status_code == 200
            assert response.json == \
                {'status': 'RUNNING', 'result': 'Loading ...'}

    @pytest.mark.usefixtures('set_task_id_in_session')
    def test_result_error(self, client, failed_async_result):
        with mock.patch('app.controllers.get_async_result') as async:
            async.return_value = failed_async_result
            response = client.get('/loading')
            assert response.status_code == 200
            assert response.json == {'status': 'ERROR', 'result': 'Try again'}
